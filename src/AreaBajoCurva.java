import java.math.*;
import java.util.Calendar;
/**
 * @Autor internet.
 * @Modificado Sergio Martinez.
 * Si la rueda ya esta hecha para que volverla a hacer.
 * Se lee el codigo y se analiza para hacer las diferentes funciones x ^ n.
 * 
 * Se comentan diferentes funciones con el // para poder usarlar descomentar y comentar.
 * 
 * Si se cambia el intervalo debe ser en     x=integralx(1,10,n); // Intervalos de 1 a 10
 * 														 ^,^,
 * 														1 y 2	
 * 
 * Al final ejecutar el programa, se puede verificar con calculadora.
 **/
public class AreaBajoCurva {
	
       public static double integralsup(int a, int b, long n) {
            double base, suma=0, y=0, f=0, xelevar=0;
                int i;
                base=(double)(b-a)/n;
                for(i=0;i<n;i++){
	               y=base*(i+1)+a;
	               xelevar = Math.pow(y,2); // elevar a 2 - 1 y 2
	               //xelevar = Math.pow(y,6); // elevar a 6 - 3
	               		f = xelevar+2*y-3; //= x^2+2Y-3
	               		//f = (-xelevar)-1; //= -x^2-1
	                    //f = 3*xelevar-8*y; //= 3(X)^6-8Y
	                       suma+=base*f;
                }
            return suma;
       }
       
       public static double integralinf(int a, int b, long n) {
            double base, suma=0, y=0, f=0, xelevar=0;
                int i;
                base=(double)(b-a)/n;
                for(i=0;i<n;i++){
	               y=base*(i)+a;
	               xelevar = Math.pow(y,2); // elevar a 2 - 1 y 2
	               //xelevar = Math.pow(y,6); // elevar a 6 - 3
	               		f = xelevar+2*y-3; //= x^2+2Y-3
	               		//f = (-xelevar)-1; //= -x^2-1
	                    //f = 3*xelevar-8*y; //= 3(X)^6-8Y
	                       suma+=base*f;
                }
            return suma;
       }
       
       public static void main(String[] args) {
               // TODO Auto-generated method stub 
    	   		long time_start, time_end;
    	   		time_start = System.currentTimeMillis();
	           double error,inf,sup;
	           error = 1;
	           sup = 0;
	           inf=0;
	           long n =0;
	           long i = 0;
	           for(n=10000;n>10;n=n+1000){
	        	   //System.out.println("n = "+n);
	                       sup=integralsup(1,-1,n); // Intervalos de 1 a 10
	                       inf=integralinf(1,-1,n); // Intervalos de 1 a 10
	                       error=sup-inf;
	                       //System.out.println("i = "+i);
	                       //System.out.println("error = "+error);
	               i=n;
	               if (error<20) break;
	            }
           System.out.println("Res para Superior: "+sup);
           System.out.println("Res para Inferior: "+inf);
           //System.out.println("Error: "+error);
           //System.out.println("Numero de iteraciones: "+i); // Iteraciones en operaciones
           time_end = System.currentTimeMillis();
   	    	System.out.println("tomo "+ ( time_end - time_start ) +" millisegundos");
       }
}